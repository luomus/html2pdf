<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use Vixriihi\Html2Pdf;

$html = file_get_contents('php://input');
if (empty($html)) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true, 404);
    exit();
}

$html2pdf = new Html2Pdf();
$defaults = [
    'dpi' => 96,
    'page-size' => 'A4',
    'print-media-type' => '',
    'encoding' => 'utf-8',
    'margin-bottom' => 0,
    'margin-left' => 0,
    'margin-right' => 0,
    'margin-top' => 0,
];

$filename = isset($_GET['filename']) ? $_GET['filename'] : 'generated';
$filename = $html2pdf->clearValue($filename);
$options  = array_replace($defaults, $_GET);
$html2pdf->setOptionsFromArray($options,
    [
        'dpi',
        'disable-smart-shrinking',
        'print-media-type',
        'encoding',
        'margin-bottom',
        'margin-left',
        'margin-right',
        'margin-top',
        'page-size',
        'orientation',
        'zoom',
    ]);
$pdf = $html2pdf->convert($html);
if ($pdf === null) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    exit();
}

header("Content-type: application/octet-stream");
header("Content-disposition: attachment;filename=$filename.pdf");

echo $pdf;