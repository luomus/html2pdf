# Html to pdf

This is a php wrapper for [wkhtmltopdf](http://wkhtmltopdf.org/).

## Requirements

 * php > 5.4.0
 * [wkhtmltopdf](http://wkhtmltopdf.org/downloads.html)
 * [composer](https://getcomposer.org/)

## Installing

After installing all the requirements run ```php composer.phar install``` in the root of the directory
where you want to install this. And your done.

## Composer

Composer is a package management system designed for php. The only package that we need is the wrapper
classes. They can be found [here](https://github.com/vixriihi/html2pdf).


